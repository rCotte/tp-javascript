const app = document.querySelector('#app');
const button = document.querySelector('button');

async function fetchPerson(index) {
    // Person request
    const response = await fetch(`https://swapi.dev/api/people/${index}`);
    const person = await response.json();
    // On test si la requête fonctionne
    if (!response.ok) {
        return null;
    }
    // Planet request
    const homeWorldReq = await fetch(person.homeworld);
    const world = await homeWorldReq.json();

    addPersonToDOM(person, world);
    return person;
}

function addPersonToDOM(person, world) {
    const personDiv = document.createElement('div');
    const worldDiv = document.createElement('span');

    personDiv.textContent = person.name;
    personDiv.classList.add('person');
    worldDiv.textContent = ' Lives in: ' + world.name;

    personDiv.appendChild(worldDiv);
    app.appendChild(personDiv);
}

let i = 0;
button.addEventListener('click', async () => {
    button.disabled = true;
    button.textContent = 'Loading';
    const person = await fetchPerson(i);

    if (!person) {
        console.error('Une erreur est survenue avec la requete');
        alert('Une erreur est survenue avec la requete');
    }

    button.disabled = false;
    button.textContent = 'Fetch another';
    i++;
});
