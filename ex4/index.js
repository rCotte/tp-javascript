const btn = document.querySelector('#btn');
const input = document.querySelector('input');
const form = document.querySelector('form');

input.addEventListener('change', event => {
    if (!input.value || input.value !== '123') {
        input.setCustomValidity('Le champ doit contenir 123');
        input.reportValidity();
    }
});

form.addEventListener('submit', event => {
    event.preventDefault();
});