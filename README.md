# Javascript

Lien du cours : https://slides.com/mrman/javascript

Ce projet contient les corrections des exercices donnés en cours.

## Exercice

Vous vous baserez sur la correction de l'exercice 5, qui utilise l'API : https://swapi.dev/

### Première partie - Ajout du monde d'origine

En plus de connaitre le nom des personnes, on aimerait afficher leur planète d'origine.  

Pour cela : 

- Se servir de la clef 'homeworld' de la personne
- Pour faire une nouvelle reqête et ainsi récupérer la planète
- Rajouter un paramètre à la fonction `addPersonToDOM` afin d'afficher également la planète
  - Comme pour la personne, créer une nouvelle balise de type `span` au lieu de `div` ici
  - Ajouter le nom de la planête dans le contenu du `span`
  - Ajouter le `span` dans les enfants de la `div` person

### Deuxième partie - Gestion de l'asynchronicité

Le code tel qu'il est maintenant comporte un petit bug, si l'utilisateur clique rapidement deux fois d'affilé, la même personne peut être récupérer deux fois.

Faire en sorte que :

- Avant qu'on lance la requête éditer le bouton pour 
  - Ajouter l'attribut `disabled` à `true` pour désactiver le bouton
  - Changer le contenu du bouton en `Loading...`
- Après que la requête soit finie
  - Remmettre l'attribut `disabled` à `false`
  - Changer le contenu du bouton en `Fetch another`

### Troisième partie - Gestion d'erreur

Dans le code javascript, changez la déclaration de i pour avoir :

```js
let i = 0;
```

Si vous relancez une requête la première devrait générer une erreur avec un code 404. En effet, les indexs de l'API commencent à 1.

A partir de là, gérer l'erreur pour l'afficher à l'utilisateur grace à la fonction globale `alert(message)`, afficher aussi l'erreur en console avec `console.error(message)`.

Faire en sorte que même après une erreur, l'index s'incrémente correctement et la première personne s'affiche.